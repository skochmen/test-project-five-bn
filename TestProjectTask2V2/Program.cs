﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProjectTask2V2
{
    class CoupleNumbers
    {
        public int a;
        public int b;
        public int result;
    }

    class Program
    {
        private static List<int> arr = new List<int>();
        private static List<int> leftArr = new List<int>();
        private static List<int> rightArr = new List<int>();
        private static Random rand = new Random();
        private static List<CoupleNumbers> cNumbers = new List<CoupleNumbers>();
        static void ListGenerator(List<int> list, int count)
        {
            for (int i = 0; i < count; i++)
            {
                list.Add(rand.Next(0, 77));
            }
        }
        private static void GetListCoupleNumders()
        {
            for (int i = 0; i < leftArr.Count; i++)
            {
                for (int j = 0; j < rightArr.Count; j++)
                {
                    if (leftArr[i] < rightArr[j])
                    {
                        cNumbers.Add(new CoupleNumbers() { a = leftArr[i], b = rightArr[j], result = rightArr[j] - leftArr[i] });
                    }
                }
            }
            cNumbers.Sort((x, y) => x.result.CompareTo(y.result));
        }
        static void Main(string[] args)
        {
            //ListGenerator(arr, 100);

            Console.WriteLine("Введите данные массива через пробел :");
            int[] dat = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            foreach (var x in dat)
            {
                arr.Add(x);
            }

            arr.Sort();

            for (int i = 0; i < arr.Count; i++)
            {
                if (leftArr.Count < arr.Count / 2d)
                {
                    leftArr.Add(arr[i]);
                }
                else
                {
                    rightArr.Add(arr[i]);
                }
            }

            string str = string.Join(" ", arr);
            Console.WriteLine("Начальный массив " + str);
            Console.WriteLine("Половина суммы начального массива: " + arr.Sum() / 2d);
            str = string.Join(" ", leftArr);
            Console.WriteLine("Левый массив  " + str);
            str = string.Join(" ", rightArr);
            Console.WriteLine("Правый массив " + str);

            GetListCoupleNumders();

            for (int i = cNumbers.Count - 1; i >= 0; i--)
            {
                if (cNumbers[i].result <= (arr.Sum() / 2 - leftArr.Sum()))
                {
                    leftArr.Add(cNumbers[i].b);
                    leftArr.Remove(leftArr.Find(x => x == cNumbers[i].a));
                    leftArr.Sort();
                    rightArr.Add(cNumbers[i].a);
                    rightArr.Remove(rightArr.Find(x => x == cNumbers[i].b));
                    rightArr.Sort();
                    cNumbers.Clear();
                    GetListCoupleNumders();
                    i = cNumbers.Count - 1;
                }
            }

            str = string.Join(" ", leftArr);
            Console.WriteLine("\n\nРезульт левый массив " + str);
            Console.WriteLine("Сумма левого массива: " + leftArr.Sum());
            str = string.Join(" ", rightArr);
            Console.WriteLine("Результат правый массив " + str);
            Console.WriteLine("Сумма правого массива: " + rightArr.Sum());
        }
    }
}
