﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProjectTask2
{
    class Program
    {
        public class Class1
        {
            private static List<int> arr = new List<int>();
            private static List<int> arrResult1 = new List<int>();
            private static List<int> arrResult2 = new List<int>();
            static void Main(string[] args)
            {
                Console.WriteLine("Введите данные массива через пробел :");
                int[] dat = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

                foreach (var x in dat)
                {
                    arr.Add(x);
                }
                Console.WriteLine($"Сумма вводного массива : {arr.Sum()}");

                int S = arr.Sum();
                if (S % 2 != 0)
                {
                    arr.Sort();
                    double average = arr.Sum() / 2d;

                    var str = string.Join(" ", arr);
                    Console.WriteLine("Вводный массив  " + str);
                    Console.WriteLine($"Половина суммы массива {average}");

                    for (int i = arr.Count - 1; i != 0; i--)
                    {
                        if (arr[i] <= average - arrResult1.Sum())
                        {
                            arrResult1.Add(arr[i]);
                            arr.Remove(arr[i]);
                        }
                        else
                        {
                            int y = arr.Find(x => x <= average - arrResult1.Sum());
                            arr.Remove(y);
                            if (y != 0) arrResult1.Add(y);
                            arrResult2 = arr;
                        }
                    }

                    str = string.Join(" ", arrResult1);
                    Console.WriteLine("Левый массив  " + str);
                    Console.WriteLine("Сумма первого массива  " + arrResult1.Sum());
                    str = string.Join(" ", arrResult2);
                    Console.WriteLine("Правый массив  " + str);
                    Console.WriteLine("Сумма вторго массива  " + arrResult2.Sum());
                }
                else if (S % 2 == 0)
                {
                    S /= 2;
                    int[,] M = new int[S + 1, S + 1];
                    M[0, 0] = -1;

                    foreach (int x in dat)
                    {
                        for (int a = S; a >= 0; a--)
                        {
                            for (int b = S; b >= 0; b--)
                            {
                                if (M[a, b] == 0)
                                {
                                    if (a >= x && M[a - x, b] != 0)
                                    {
                                        M[a, b] = x;
                                    }
                                    else if (b >= x && M[a, b - x] != 0)
                                    {
                                        M[a, b] = -x;
                                    }
                                }
                            }
                        }
                    }

                    if (M[S, S] == 0)
                    {
                        Console.WriteLine("-1"); return;
                    }

                    int u = S, v = S;

                    while (u != 0 || v != 0)
                    {
                        if (M[u, v] > 0)
                        {
                            arrResult1.Add(M[u, v]);
                            u -= M[u, v];
                        }
                        else
                        {
                            arrResult2.Add(-M[u, v]);
                            v += M[u, v];
                        }
                    }

                    string str = string.Join(" ", arrResult1);
                    Console.WriteLine($"Первый массив : {str}");
                    Console.WriteLine($"Сумма первого массива :{arrResult1.Sum()}");
                    str = string.Join(" ", arrResult2);
                    Console.WriteLine($"Второй массив : {str}");
                    Console.WriteLine($"Сумма второго массива :{arrResult2.Sum()}");
                }
            }
        }
    }
}

