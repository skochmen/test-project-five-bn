﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestProjectTask1
{
    class Program
    {

        private static List<double> arr = new List<double>();
        private static List<double> arr2 = new List<double>();
        private static List<double> arrResult = new List<double>();

        static void Main(string[] args)
        {
            //GenerateList(10);
            Console.WriteLine("Введите данные массива через пробел :");
            int[] dat = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            foreach (var x in dat)
            {
                arr.Add(x);
            }

            for (int i = 0; i < arr.Count - 1; i++)
            {
                arr2.Add((arr[i] + arr[i+1]) / 2);
            }

            for (int i = 0; i < arr.Count; i++)
            {
                arrResult.Add(arr[i]);
                if (i < arr2.Count)
                {
                    arrResult.Add(arr2[i]);
                }
            }

            string str = string.Join(" ", arrResult);
            Console.WriteLine("Результат : " + str);
        }
    }
}
